<a href="https://openhexagon.org" target="_blank">
    <p align="center">
        <img src="https://vittorioromeo.info/Misc/Linked/githubohlogo2.png">
    </p>
</a>

[![pipeline status](https://gitlab.com/her0/open-hexagon-ci/badges/ci-experiments/pipeline.svg)](https://gitlab.com/her0/open-hexagon-ci/-/pipelines/ci-experiments/latest) [![linux download](https://gitlab.com/her0/open-hexagon-ci/badges/ci-experiments/pipeline.svg?job=linux-package&key_text=Linux+Build+%F0%9F%93%A6&key_width=90)](https://gitlab.com/her0/open-hexagon-ci/-/jobs/artifacts/ci-experiments/raw/OpenHexagon.tar.gz?job=linux-package)

> **Four buttons, one goal: survive. Are you ready for a real challenge? -- Become the best and climb the leaderboards in Open Hexagon, a fast-paced adrenalinic arcade game that's easy to learn but hard to master!** *Now available on [Steam](https://store.steampowered.com/app/1358090/)*.

<a href="https://www.youtube.com/watch?v=06y7mEsAMHM" target="_blank">
    <p align="center">
        <img src="https://img.youtube.com/vi/06y7mEsAMHM/0.jpg">
    </p>
</a>   

## Credits

### Original Inspiration

- Open Hexagon is inspired by the excellent [Super Hexagon](https://store.steampowered.com/app/221640/Super_Hexagon/), created by [Terry Cavanagh](https://distractionware.com/). 
- **Terry [fully supports Open Hexagon](https://twitter.com/terrycavanagh/status/1397373413329571845) -- thank you!**

### Source Contributors

- John Kline
- Zly | [@zly_u](https://twitter.com/zly_u)
- AlphaPromethium
- Misa "Info Teddy" Elizabeth | [@Info__Teddy](https://twitter.com/Info__Teddy)
- Ivory Duke
- MultHub

### Testing

- [Maniac](https://www.youtube.com/channel/UCnEHReBWFQ_0_-Ro4TpH4Tw)

## Building

Building instructions are available on [the wiki](https://github.com/SuperV1234/SSVOpenHexagon/wiki/Building-Instructions).
